import React, { useState } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import Navigator from './src/screens/navigator';
import { Provider } from 'react-redux';
import Store from './src/redux/store';
import { colors } from './src/global/colors';
import RoughScreen from './src/screens/rough';


LogBox.ignoreAllLogs(true);



const App = () => {
    return (
        <Provider store={Store}>
            <NavigationContainer>
                <StatusBar backgroundColor = "#0D0D0D"   />  
                <Navigator />
               {/* <RoughScreen /> */}
            </NavigationContainer>
        </Provider>
    )
}

export default App
