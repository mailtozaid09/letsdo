import { ADD_TODO_LIST, DELETE_TODO_LIST, UPDATE_TODO_LIST } from './HomeActionTypes';


const initialState = {
    todo_list: [],
};


export default HomeReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO_LIST: {
            const {
                id, 
                taskName,
                time,
                date, 
                category, 
                isCompleted,
                isImportant,
            } = action.payload
            return {
                ...state,
                todo_list: [
                    ...state.todo_list, {
                        id, 
                        taskName,
                        time,
                        date, 
                        category, 
                        isCompleted,
                        isImportant,
                    }
                ]
            };
        }

        case UPDATE_TODO_LIST:{
            const { taskName,time, date, category, isCompleted, isImportant } = action.payload
            const updatedTodo = state.todo_list.map(todo => {
                if (todo.id != action.payload.id) {
                  return todo;
                } else {
                  return {
                    ...todo, 
                    taskName: taskName,
                    time: time,
                    date: date,
                    category: category,
                    isCompleted: isCompleted,
                    isImportant: isImportant,
                  };
                }
              });
           
            return {
                ...state,
                todo_list: updatedTodo
            };}

        case DELETE_TODO_LIST: {
            const { id } = action.payload
                return {
                    ...state,
                    todo_list: state.todo_list.filter((todo) => todo.id != id)
            };
        }



        default:
            return state;
    }
};
