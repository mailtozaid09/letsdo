import { ADD_TODO_LIST, DELETE_TODO_LIST, UPDATE_TODO_LIST } from "./HomeActionTypes";

let nextTodoId = 0;

export const addTodoList = params => {
    return {
        type: ADD_TODO_LIST,
        payload: {
            id: ++nextTodoId,
            taskName: params.taskName,
            time: params.time,
            date: params.date,
            category: params.category,
            isCompleted: params.isCompleted,
            isImportant: params.isImportant,
        },
    };
};

export const deleteTodoList = id => {
    return {
        type: DELETE_TODO_LIST,
        payload: {
            id
        },
    };
};


export const updateTodoList = params => {

    return {
        type: UPDATE_TODO_LIST,
        payload: {
            id: params.id,
            taskName: params.taskName,
            time: params.time,
            date: params.date,
            category: params.category,
            isCompleted: params.isCompleted,
            isImportant: params.isImportant,
        },
    };
};
