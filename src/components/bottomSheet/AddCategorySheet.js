import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import RBSheet from "react-native-raw-bottom-sheet";
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import LoginButton from '../button/LoginButton';
import Input from '../input';

const AddCategorySheet = ({refRBSheet, addNewCategory}) => {


    const categories = [
        {
            "categoryName": 'Daily Tasks',
            "icon": require('../../../assets/images/daily.png'),
        },
        {
            "categoryName": 'School Tasks',
            "icon": require('../../../assets/images/cart.png'),
        },
        {
            "categoryName": 'Shopping List',
            "icon": require('../../../assets/images/school.png'),
        },
        {
            "categoryName": 'Work',
            "icon": require('../../../assets/images/heart.png'),
        },
        {
            "categoryName": 'Personal',
            "icon": require('../../../assets/images/calendar_icon.png'),
        },
        {
            "categoryName": 'Workout',
            "icon": require('../../../assets/images/time.png'),
        }
    ]

  
    const [categoryValue, setCategoryValue] = useState('');
    const [todoTaskName, setTodoTaskName] = useState();


    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                height={650}
                onClose={() => { setTodoTaskName(''); setCategoryValue('')}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    }, 
                    container: {
                        backgroundColor: colors.white,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        padding: 20,
                    }
                }}
            >
                <View>
                    <Text style={styles.heading} >Category</Text>

                    <Input
                        label="Todo Task Name"
                        placeholder="Todo Task Name"
                        value={todoTaskName}
                        onChangeText={(text) => {setTodoTaskName(text);}}
                    />

                    <Input
                        label="Category Name"
                        placeholder="Category Name"
                        value={categoryValue}
                        onChangeText={(text) => {setCategoryValue(text); }}
                    />

                    <Text style={styles.subheading} >Or select existing category</Text>


                    <View style={styles.categoryContainer} >
                        <View style={styles.categoryItems} >
                            {categories.map((item, index) => (
                                <TouchableOpacity
                                key={index}
                                onPress={() => {setCategoryValue(item.categoryName)}}
                                style={{height: 100, width: 100, marginBottom: 20, borderRadius: 20, borderWidth: 1, borderColor: categoryValue == item.categoryName ? colors.primary : colors.light_purple,  backgroundColor: colors.light_purple, alignItems: 'center', justifyContent: 'center'}}  >
                                <View style={{alignItems: 'center', justifyContent: 'center'}} >
                                    <Image source={item.icon} style={{height: 30, width: 30, marginBottom: 5}} />
                                    <Text style={{fontSize: 13, color: categoryValue == item.categoryName ? colors.primary : colors.gray, fontFamily: Poppins.Medium}} >{item.categoryName}</Text>
                                </View>
                            </TouchableOpacity>
                            ))}
                        </View>

                    </View>


                    <LoginButton
                        title="Create Category" 
                        onPress={() => {
                            addNewCategory(categoryValue, todoTaskName)
                        }}
                        disabled={todoTaskName && categoryValue ? false : true}
                        textStyle={{color: colors.white}}
                        buttonStyle={{backgroundColor: todoTaskName && categoryValue ? colors.primary : colors.gray, marginBottom: 15}}
                    />
                                
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    heading: {
        fontSize: fontSize.SubHeading,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginBottom: 15,
        textAlign: 'center'
    },
    subheading: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginBottom: 15,
        textAlign: 'center'
    },
    categoryContainer: {
        // flexDirection: 'row',
        // alignItems: 'center',
        //justifyContent: 'space-between'
    },
    categoryItems: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between'
        //alignItems: 'center',
    },
    saveText: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.gray,
    }
    
})

export default AddCategorySheet