import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import RBSheet from "react-native-raw-bottom-sheet";
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import Input from '../input';

const AddTodoSheet = ({refRBSheet, addNewTodo}) => {

    const [taskValue, setTaskValue] = useState('');
    const [currentCat, setCurrentCat] = useState('Daily Task');

    const [isImportant, setIsIportant] = useState(false);

    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                onClose={() => {setCurrentCat('Daily Task'); setTaskValue('')}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    }, 
                    container: {
                        backgroundColor: colors.white,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        padding: 20,
                    }
                }}
            >
                <View>
                    <Text style={styles.heading} >New Task</Text>

                    <Input
                        placeholder="New Task"
                        value={taskValue}
                        onChangeText={(text) => setTaskValue(text)}
                    />


                    <View style={styles.categoryContainer} >
                        <View style={styles.categoryItems} >
                            <TouchableOpacity
                                onPress={() => setIsIportant(!isImportant)}
                                style={{height: 40, width: 40, borderRadius: 20,  marginRight: 15, backgroundColor: isImportant ? colors.primary : colors.white, alignItems: 'center', justifyContent: 'center'}}  >
                                <Image source={isImportant ? media.star : media.star_inactive} style={{height: 20, width: 20,}} />
                            </TouchableOpacity>

                            <TouchableOpacity
                                //onPress={() => setIsIportant(!isImportant)}
                                style={{height: 40, width: 40, borderRadius: 20,  marginRight: 15, alignItems: 'center', justifyContent: 'center'}}  >
                                <Image source={media.calendar_icon} style={{height: 20, width: 20,}} />
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity
                            onPress={() => addNewTodo(currentCat, taskValue, isImportant)}
                            disabled={taskValue ? false : true}
                        >
                            <Text style={[styles.saveText, taskValue ? {color: colors.primary, fontFamily: Poppins.SemiBold} : {}]} >Save</Text>
                        </TouchableOpacity>
                    </View>
                            
                </View>
            </RBSheet>
        </View>
    )
}

const styles = StyleSheet.create({
    heading: {
        fontSize: fontSize.SubHeading,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    categoryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    categoryItems: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    saveText: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.gray,
    }
    
})

export default AddTodoSheet