import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';

const Input = ({ placeholder, isSearch, label, value,onClearSearchText, onChangeText, onChangeEyeIcon, isPassword, showEyeIcon }) => {
    return (
        <View>

        <Text style={styles.label} >{label}</Text>

        <View style={[styles.inputContainer]}  >

            {isSearch && (
                <TouchableOpacity disabled={true} onPress={onChangeEyeIcon}>
                    <Image source={media.search} style={[styles.iconImage, {marginRight: 15}]} />
                </TouchableOpacity>
            )}

            <TextInput
                placeholder={placeholder}
                style={styles.inputStyle}
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={isPassword  && !showEyeIcon ? true : false}
            />

            {isPassword && (
                <TouchableOpacity onPress={onChangeEyeIcon}>
                    <Image source={showEyeIcon ? media.visible : media.invisible} style={styles.iconImage} />
                </TouchableOpacity>
            )}

            {isSearch && value && (
                <TouchableOpacity onPress={onClearSearchText}>
                    <Image source={media.close} style={[styles.iconImage, {position: 'absolute', right: -5}]} />
                </TouchableOpacity>
            )}
        </View>
        </View>
    )
}


const styles = StyleSheet.create({
    inputContainer: {
        height: 60,
        padding: 20,
        backgroundColor: colors.light_purple,
        borderRadius: 10,
        width: screenWidth-40,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Medium,
        color: colors.black,
        width: screenWidth-120,
    },
    iconImage: {
        height: 24,
        width: 24
    },
    label: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
        marginBottom: 6,
    }
})

export default Input