import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';

const LoginButton = ({title, onPress, buttonStyle, textStyle, disabled }) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.buttonContainer, buttonStyle]}
            disabled={disabled}
        >
            <Text style={[styles.buttonText, textStyle]} >{title}</Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    buttonContainer: {
        height: 60,
        backgroundColor: colors.white,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40
    },
    buttonText: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Medium,
        color: colors.black
    }
})

export default LoginButton