import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, Image, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';

const AddButton = ({title, onPress, buttonStyle, textStyle, }) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.buttonContainer, buttonStyle]}
        >
            <Image source={media.plus} style={styles.imageStyle} />
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    buttonContainer: {
        height: 50,
        width: 50,
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.primary,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Medium,
        color: colors.black
    },
    imageStyle: {
        height: 20,
        width: 20
    }
})

export default AddButton