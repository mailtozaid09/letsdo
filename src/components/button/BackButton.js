import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, Image, } from 'react-native';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';

const BackButton = ({onPress, }) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={styles.buttonContainer}
        >
            <Image source={media.left_arrow} style={styles.iconImage} />
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    buttonContainer: {
        height: 50,
        width: 50,
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.gray,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconImage: {
        height: 24,
        width: 24
    }
})

export default BackButton