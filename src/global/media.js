export const media = {
    onboarding: require('../../assets/images/onboarding.png'),
    getstarted: require('../../assets/images/getstarted.png'),

    visible: require('../../assets/images/visible.png'),
    invisible: require('../../assets/images/invisible.png'),
    
    tick: require('../../assets/images/tick.png'),
    delete: require('../../assets/images/delete.png'),
    
    left_arrow: require('../../assets/images/left-arrow.png'),
    down_arrow: require('../../assets/images/down-arrow.png'),
    up_arrow: require('../../assets/images/up-arrow.png'),

    dots: require('../../assets/images/dots.png'),
    menu: require('../../assets/images/menu.png'),
    edit: require('../../assets/images/edit.png'),
    edit_white: require('../../assets/images/edit_white.png'),

    oops: require('../../assets/images/oops.png'),

    search: require('../../assets/images/search.png'),
    close: require('../../assets/images/close.png'),

    calendar: require('../../assets/images/calendar.png'),
    calendar_inactive: require('../../assets/images/calendar_inactive.png'),
    calendar_icon: require('../../assets/images/calendar_icon.png'),

    star: require('../../assets/images/star.png'),
    star_inactive: require('../../assets/images/star_inactive.png'),

    time: require('../../assets/images/time.png'),
    logout: require('../../assets/images/logout.png'),
    home: require('../../assets/images/home.png'), 

    school: require('../../assets/images/book.png'),
    heart: require('../../assets/images/heart.png'), 
    daily: require('../../assets/images/daily.png'), 
    plus: require('../../assets/images/plus.png'),
    plus_white: require('../../assets/images/plus_white.png'),
    cart: require('../../assets/images/cart.png'), 

    userProfile: require('../../assets/images/userProfile.jpeg'),
    user1: require('../../assets/images/user1.jpeg'),
    user2: require('../../assets/images/user2.jpeg'),
}