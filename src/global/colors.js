export const colors = {

    primary: '#9088d4',
    dark_purple: '#716897',
    light_purple: '#e7e6f5',
    black: '#252a34',
    white: '#fdfdfd',
    gray: '#919399',
    blue: '#81b3f3',
    red: '#f29999',
    orange: '#f7c191',
    reddish: '#ED4545',

}