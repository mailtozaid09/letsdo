import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import OnBoardingScreen from '../onboarding';
import GetStartedScreen from '../onboarding/GetStarted';
import LoginScreen from '../login';
import SignUpScreen from '../signup';

const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {
    return(
        <Stack.Navigator initialRouteName="OnBoarding" >
            <Stack.Screen
                name="OnBoarding"
                component={OnBoardingScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="GetStarted"
                component={GetStartedScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="SignUp"
                component={SignUpScreen}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    )
}

export default LoginStack