import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import LoginStack from './LoginStack';
import DrawerStack from './DrawerStack';
import TaskStack from './TaskStack';

import ProfileScreen from '../profile';


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();



const Navigator = ({navgation}) => {


    return (
        <Stack.Navigator 
        initialRouteName="Drawer" 
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Drawer"
                component={DrawerStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="TaskStack"
                component={TaskStack}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    );
}

export default Navigator