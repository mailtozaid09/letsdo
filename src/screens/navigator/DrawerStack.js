import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../home';
import DrawerSidebarMenu from './DrawerSidebarMenu';
import { media } from '../../global/media';
import { fontSize, Poppins } from '../../global/fontFamily';
import { colors } from '../../global/colors';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


const DrawerStack = () => {

    function DetailsScreen({ navigation }) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
              onPress={() => navigation.navigate('Profile')}
              title="Go to notifications"
            />
          </View>
        );
      }
      function ProfileScreen({ navigation }) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button
              onPress={() => navigation.navigate('Notifications')}
              title="Go to notifications == ProfileScreen"
            />
          </View>
        );
      }
      
      function NotificationsScreen({ navigation }) {
        return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button onPress={() => navigation.goBack()} title="Go back home" />
          </View>
        );
      }

      return (
        <Drawer.Navigator 
            drawerContent={(props) => <DrawerSidebarMenu {...props} />}
            initialRouteName="Home"
            screenOptions={({ navigation }) => ({
              headerLeft: props => <TouchableOpacity onPress={() => navigation.openDrawer()} >
                <Image  source={media.menu} style={{height: 30, width: 30, marginLeft: 20, marginBottom: 10}} />
              </TouchableOpacity>,
            })}
        >
            <Drawer.Screen name="Home" component={HomeScreen} 
              options={{
                headerShown: true,
                headerStyle: {backgroundColor: colors.primary, height: 100},
                headerTitleStyle: {
                  fontSize: fontSize.Title,
                  fontFamily: Poppins.Medium,
                  color: colors.white,
                  marginBottom: 20
                }
            }}
            />
            <Drawer.Screen name="Notifications" component={NotificationsScreen} />
        </Drawer.Navigator>
    );
}

export default DrawerStack