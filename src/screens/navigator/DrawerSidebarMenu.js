import { DrawerContentScrollView } from '@react-navigation/drawer';
import React from 'react'
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, Pressable, } from 'react-native';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';


const DrawerSidebarMenu = (props) => {
    return (
        <SafeAreaView style={{flex: 1, backgroundColor: colors.primary}} >
            
                <View style={styles.detailsContainer} >

                  

                    <Image source={media.user2} style={{height: 120, width: 120, borderRadius: 60, margin: 12}} />
                    
                    <View style={{flexDirection: 'row'}} >

                       <View style={{alignItems: 'center'}} >
                            <Text style={styles.detailsTitle}>Zaid Ahmed</Text>
                            <Text style={styles.detailsSubTitle}>mailtozaid09@gmail.com</Text>
                       </View>

                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('Profile')}
                        >
                            <Image source={media.edit_white} style={{height: 22, width: 22,}} />
                        </TouchableOpacity>
                    </View>
               
                </View>

                

            <DrawerContentScrollView {...props} contentContainerStyle={{flex: 1}} >

                                    
                <View style={{flex: 1, justifyContent: 'space-between'}} >
                    <View>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate("Home")}
                            style={styles.drawerItem}
                            >
                            <Image source={media.home} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>Home</Text>
                        </TouchableOpacity>

                        <TouchableOpacity


                            onPress={() => {
                                props.navigation.navigate('TaskStack', {
                                    screen: 'SideTask', 
                                    params: {
                                        categoryName: 'Today', 
                                        filterBy: 'TodaysTask', 
                                    }
                                });
                            }}

                            style={styles.drawerItem}
                            >
                            <Image source={media.time} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>Today</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {props.navigation.navigate('Tasks', 
                            {task: [{
                                "id": 4,
                                "category": 'Important',
                                "icon": media.heart,
                                "bgColor": colors.orange,
                                "taskName": "Go to Market",
                                "date": "Today",
                                "time": "01:46",
                                "completed": false,
                            },], categoryName: 'Important'})}}
                            style={styles.drawerItem}
                            >
                            <Image source={media.star} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>Important</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {props.navigation.navigate('Tasks', {task: [
                            ], categoryName: 'Calendar'})}}
                            style={styles.drawerItem}
                            >
                            <Image source={media.calendar} style={{height: 26, width: 26, marginRight: 12}} />
                            <Text style={styles.drawerItemText}>Calendar</Text>
                        </TouchableOpacity>
                    </View>


                    <TouchableOpacity
                        onPress={() => props.navigation.navigate("LoginStack")}
                        style={styles.drawerItem}
                    >
                        <Image source={media.logout} style={{height: 26, width: 26, marginRight: 12}} />
                        <Text style={styles.drawerItemText}>Log Out</Text>
                    </TouchableOpacity>
                </View>
            </DrawerContentScrollView>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    drawerItem: {
        padding: 10,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,

    },
    drawerItemText: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    detailsContainer: {
        alignItems: 'center',
        borderBottomWidth: 1,
        paddingBottom: 20,
        borderColor: colors.white
    },
    detailsTitle: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
    detailsSubTitle: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Medium,
        color: colors.white,
    },
})
export default DrawerSidebarMenu