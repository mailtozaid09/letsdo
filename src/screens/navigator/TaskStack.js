import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import TaskScreen from '../tasks';
import EditTask from '../tasks/EditTask';
import SideTask from '../tasks/SideTask';

const Stack = createStackNavigator();


const TaskStack = ({navgation}) => {
    return(
        <Stack.Navigator initialRouteName="Tasks" >
            <Stack.Screen
                name="Tasks"
                component={TaskScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="EditTask"
                component={EditTask}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="SideTask"
                component={SideTask}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    )
}

export default TaskStack