import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, Alert, Modal, Pressable, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { screenHeight, screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';


import BackButton from '../../components/button/BackButton';
import LoginButton from '../../components/button/LoginButton';

import Input from '../../components/input';
import { useDispatch } from 'react-redux';
import { deleteTodoList, updateTodoList } from '../../redux';
import moment from 'moment';



const EditTask = (props) => {

    const dispatch = useDispatch();
    const navigation = props.navigation;

    const params = props?.route?.params

    const [taskName, setTaskName] = useState(params?.taskDetails.taskName);

    useEffect(() => {
            
    }, [])


    const onDeleteTask = (task) => {
        dispatch(deleteTodoList(task.id))
        navigation.goBack()
    }

    const onUpdateTask = (task) => {

        const newdate = new Date();

        var date = moment(newdate).format("dddd, MMM Do YYYY");
        var time = moment(newdate).format("h:mm");

        dispatch(updateTodoList({
            id: task.id,
            taskName: taskName,
            time: time,
            date: date,
            category: params?.taskDetails?.category,
            isCompleted: false,
            isImportant: params?.taskDetails?.isImportant,
        }))
        
        navigation.goBack()
    }

    
    

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: colors.white}} >
            <View style={{padding: 20,flex: 1,}} >
                
                <View style={styles.headerContainer} >
                    <BackButton onPress={() => {navigation.goBack()}} />

                    <Text style={styles.heading} >Edit Task</Text>

                    {/* <TouchableOpacity onPress={() => {setMenuOptions(true)}} >
                        <Image source={media.dots} style={{height: 30, width: 20}} />
                    </TouchableOpacity> */}

                    <Text style={{width: 30}} ></Text>
                </View>


                <View style={{alignItems: 'center', flex: 1, justifyContent: 'space-between'}} >

                  <View style={{alignItems: 'center'}} >
                  <View style={styles.dateTimeContainer} >
                        <Text style={styles.dateTimeText} >{params?.taskDetails?.date} | {params?.taskDetails?.time}</Text>
                    </View>

                    <Input
                        label={"Task Name"}
                        placeholder={"Task Name"}
                        value={taskName}
                        onChangeText={(text) => setTaskName(text)}
                    />

                  </View>

                    <View>
                        <LoginButton
                            title="Delete Task" 
                            onPress={() =>  {onDeleteTask(params?.taskDetails)}}
                            textStyle={{color: colors.primary}}
                            buttonStyle={{borderColor: colors.primary, borderWidth: 1, marginBottom: 15}}
                        />

                        <LoginButton
                            title="Update Task" 
                            onPress={() =>  {onUpdateTask(params?.taskDetails)}}
                            textStyle={{color: colors.white}}
                            buttonStyle={{backgroundColor: colors.primary, marginBottom: 0}}
                        />
                    </View>
                </View>

            </View>

         </SafeAreaView>
    )
}
 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: screenWidth,
        alignItems: 'center'
    },
    textContent: {
        width: screenWidth-50,
    },
    heading: {
        fontSize: fontSize.SubHeading,
        fontFamily: Poppins.Bold,
        color: colors.black,
    },
    headerContainer: { 
        marginBottom: 30,
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    dateTimeContainer: {
        borderRadius: 50,
        padding: 5,
        paddingHorizontal: 15,
        marginBottom: 20,
        backgroundColor: colors.primary
    },
    taskName: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    dateTimeText: {
        fontSize: 12,
        fontFamily: Poppins.Light,
        color: colors.white,
    }
   

})

export default EditTask
