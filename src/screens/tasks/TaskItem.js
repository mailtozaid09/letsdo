import React, { useState } from 'react'

import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';
import CheckBox from '@react-native-community/checkbox'
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';
import { screenWidth } from '../../global/constants';
import { media } from '../../global/media';


const TaskItem = ({task, incompleted, onDeleteTaskStatus, navigation}) => {

    const [isSelected, setSelection] = useState(false);

    return (
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}} >
            <View style={{flexDirection: 'row',}} >
                <View style={{marginRight: 10, marginTop: 5,}} >
                        <CheckBox
                            value={isSelected}
                            onValueChange={(value) => {
                                setSelection(!isSelected); 
                                //onDeleteTaskStatus(); 
                            }}
                            lineWidth={2}
                            onCheckColor={colors.primary}
                            onTintColor={colors.primary}
                            onAnimationType="flat"
                            offAnimationType="bounce"
                        />
                </View>

                <View>
                    <Text style={[styles.taskName, isSelected ? {textDecorationLine:'line-through'} : {} ]} >{task.taskName}</Text>
                    <View style={styles.dateTimeContainer} >
                        <Text style={styles.dateTimeText} >{task.date} | {task.time}</Text>
                    </View>
                </View>
            </View>

            <TouchableOpacity
                onPress={() => {navigation.navigate('EditTask', {taskDetails: task})}}
            >
                <Image source={media.edit} style={{height: 25, width: 25}} />
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width: screenWidth,
        alignItems: 'center'
    },
    textContent: {
        width: screenWidth-50,
    },
    heading: {
        fontSize: fontSize.SubHeading,
        fontFamily: Poppins.Bold,
        color: colors.black,
    },
    headerContainer: { 
        marginBottom: 30,
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    dateTimeContainer: {
        borderRadius: 50,
        padding: 5,
        paddingHorizontal: 15,
        marginTop: 4,
        marginBottom: 12,
        backgroundColor: colors.primary
    },
    taskName: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    dateTimeText: {
        fontSize: 12,
        fontFamily: Poppins.Light,
        color: colors.white,
    }
   
})


export default TaskItem