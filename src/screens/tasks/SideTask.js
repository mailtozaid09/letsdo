import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, Alert, Modal, Pressable, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { screenHeight, screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';

import TaskItem from './TaskItem';
import BackButton from '../../components/button/BackButton';
import AddButton from '../../components/button/AddButton';
import AddTodoSheet from '../../components/bottomSheet/AddTodoSheet';
import moment from 'moment';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { addTodoList } from '../../redux';
import { useDispatch, useSelector } from 'react-redux';




const SideTask = (props) => {

    const dispatch = useDispatch();
    const params = props?.route?.params

    const todo_list = useSelector(state => state.HomeReducer.todo_list);
    
    const categorised_todo_list = todo_list.filter(obj => {

        var filterBy = params?.filterBy

        const newdate = new Date();

        var date = moment(newdate).format("dddd, MMM Do YYYY");

        if (filterBy == 'TodaysTask')
            return obj.date == date;
        else
            return []
    });

 
    const navigation = props.navigation;
    const refRBSheet = useRef();

    const [menuOptions, setMenuOptions] = useState(false);



    useEffect(() => {

    }, [])
    

    const addNewTodo = (currentCat, taskValue) => {
        
        const newdate = new Date();

        var date = moment(newdate).format("dddd, MMM Do YYYY");
        var time = moment(newdate).format("h:mm");

        var todoItem = {
            time: time,
            date: date,
            taskName: taskValue,
            category: params?.categoryName,
            isCompleted: false,
        }
        dispatch(addTodoList(todoItem))

        refRBSheet.current.close()
    }



    return (
        <SafeAreaView style={{flex: 1, backgroundColor: colors.white}} >
            <View style={{padding: 20, paddingBottom: 80}} >
                
                <View style={styles.headerContainer} >
                    <BackButton onPress={() => {navigation.goBack()}} />

                    <Text style={styles.heading} >{params?.categoryName}</Text>

                    <TouchableOpacity onPress={() => {setMenuOptions(true)}} >
                        <Image source={media.dots} style={{height: 30, width: 20}} />
                    </TouchableOpacity>

                    
                </View>


                <ScrollView  >
                    {categorised_todo_list.map((task) => (
                        <TaskItem navigation={navigation} task={task} onDeleteTaskStatus={() => {onDeleteTask(task)}} />
                    ))}
                </ScrollView>


                <View>
                    {categorised_todo_list.length == 0 && (
                        <View style={{height: 300, width: '100%', alignItems: 'center', justifyContent: 'center'}} >
                            <Image source={media.oops} style={{height: 250, width: 250, resizeMode: 'contain'}} />
                            <Text style={{fontSize: fontSize.Heading, fontFamily: Poppins.Light, }} >No Task Available!</Text>
                        </View>
                    )}
                </View>


                
            </View>
            <View style={styles.addButton} >
                <AddButton  onPress={() => refRBSheet.current.open()} />
            </View>

            <AddTodoSheet refRBSheet={refRBSheet} addNewTodo={(currentCat, taskValue) => addNewTodo(currentCat, taskValue)} />


            <View style={{position: 'absolute',}} >
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={menuOptions}
                        onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                        setMenuOptions(!menuOptions);
                        }}>
                        <View style={{flex: 1, backgroundColor: '#00000090',  alignItems: 'center', justifyContent:'center'}}>
                            <View style={{backgroundColor: colors.white, paddingHorizontal: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', height: 230, width: screenWidth-60}}>
                                <Text style={{fontSize: fontSize.SubHeading, fontFamily: Poppins.SemiBold,}}>Sort By</Text>
                                
                                <View style={{height: 1, width: '100%', backgroundColor: colors.gray, marginVertical: 5, }} />
                                
                                <Text style={{fontSize: fontSize.Title, marginBottom: 6, fontFamily: Poppins.Medium}}>Date Edited</Text>
                                <Text style={{fontSize: fontSize.Title, marginBottom: 6, fontFamily: Poppins.Medium}}>Date Created</Text>
                                <Text style={{fontSize: fontSize.Title, marginBottom: 6, fontFamily: Poppins.Medium}}>Title</Text>
                               

                                <Pressable onPress={() => setMenuOptions(!menuOptions)}>
                                 <Text style={{fontSize: fontSize.Title, color: colors.gray, fontFamily: Poppins.Medium, marginTop: 10}}>Cancel</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
            </View>
       
      </SafeAreaView>
    )
}
 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: screenWidth,
        alignItems: 'center'
    },
    textContent: {
        width: screenWidth-50,
    },
    heading: {
        fontSize: fontSize.SubHeading,
        fontFamily: Poppins.Bold,
        color: colors.black,
    },
    headerContainer: { 
        marginBottom: 30,
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    dateTimeContainer: {
        borderRadius: 50,
        padding: 5,
        paddingHorizontal: 15,
        marginTop: 4,
        marginBottom: 12,
        backgroundColor: colors.primary
    },
    taskName: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    dateTimeText: {
        fontSize: 12,
        fontFamily: Poppins.Light,
        color: colors.white,
    },
    addButton: {
        position: 'absolute',
        bottom: 50,
        right: 30
    }
   
})

export default SideTask
