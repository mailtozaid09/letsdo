import React, { useState } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';
import BackButton from '../../components/button/BackButton';
import LoginButton from '../../components/button/LoginButton';
import Input from '../../components/input';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';

const LoginScreen = ({navigation}) => {


    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


   
    const onChange = ({ name, value }) => {
     
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const loginFunc = () => {
        console.log("form => ", form);
        navigation.navigate('Drawer')
    }

    return (
        <SafeAreaView style={{flex: 1, alignItems: 'center', backgroundColor: colors.white}} >
            <KeyboardAvoidingView  behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.container}>
                <ScrollView  contentContainerStyle={{width: screenWidth, }} >
                    <View style={{padding: 25}} >
                
                            <View>
                                <BackButton onPress={() => {navigation.navigate('GetStarted')}} />

                                <Text style={styles.heading} >Hello Again! {'\n'}Welcome {'\n'}back </Text>
                            </View>

                            <Input
                                form={form.email}
                                placeholder="example@abc.com"
                                label="EMAIL"
                                onChangeText={(text) => onChange({name: 'email', value: text})}
                            />

                            <Input
                                form={form.password}
                                placeholder="Min. 8 characters"
                                label="PASSWORD"
                                isPassword={true}
                                showEyeIcon={showEyeIcon}
                                onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                                onChangeText={(text) => onChange({name: 'password', value: text})}
                            />
                    
                            <View style={styles.buttonContainer} >
                                <LoginButton 
                                    title="Login" 
                                    onPress={() => {loginFunc()}}
                                    textStyle={{color: colors.white}}
                                    buttonStyle={{backgroundColor: colors.primary, marginBottom: 15}}
                                />

                                <Text style={styles.alreadyText1} >DON'T HAVE AN ACCOUNT? 
                                
                                    <TouchableOpacity onPress={() => {navigation.navigate('SignUp')}} >
                                    <Text style={styles.alreadyText2} > SIGN UP</Text>
                                    </TouchableOpacity>
                                </Text>
                            </View>
                    </View>
                    
                </ScrollView>
        
            </KeyboardAvoidingView>
      </SafeAreaView>
    )
}
 

const styles = StyleSheet.create({
    container: {
        width: screenWidth,
        alignItems: 'center'
    },
    textContent: {
        width: screenWidth-50,
    },
    heading: {
        fontSize: fontSize.Heading,
        fontFamily: Poppins.Bold,
        color: colors.black,
        marginBottom: 50,
        marginTop: 50,
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 50
    },
    alreadyText1: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Light,
        color: colors.gray,
    },
    alreadyText2: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Light,
        color: colors.primary,
    },
})

export default LoginScreen


