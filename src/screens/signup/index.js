import React, { useState } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';
import BackButton from '../../components/button/BackButton';
import LoginButton from '../../components/button/LoginButton';
import Input from '../../components/input';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';

const SignUpScreen = ({navigation}) => {


    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


   
    const onChange = ({ name, value }) => {
     
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const getStartedFunc = () => {
        console.log("form => ", form);
        navigation.navigate('Drawer')
    }

    return (
        <SafeAreaView style={{flex: 1, alignItems: 'center', backgroundColor: colors.white}} >
            <KeyboardAvoidingView  behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.container}>
                <ScrollView  contentContainerStyle={{width: screenWidth, }} >
                    <View style={{padding: 20}} >
                
                            <View>
                                <BackButton onPress={() => {navigation.navigate('GetStarted')}} />

                                <Text style={styles.heading} >Hello! {'\n'}Signup to {'\n'}get started </Text>
                            </View>


                            <Input
                                form={form.name}
                                placeholder="Name"
                                label="FULL NAME"
                                onChangeText={(text) => onChange({name: 'name', value: text})}
                            />

                            <Input
                                form={form.email}
                                placeholder="example@abc.com"
                                label="EMAIL"
                                onChangeText={(text) => onChange({name: 'email', value: text})}
                            />

                            <Input
                                form={form.password}
                                placeholder="Min. 8 characters"
                                label="PASSWORD"
                                isPassword={true}
                                showEyeIcon={showEyeIcon}
                                onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                                onChangeText={(text) => onChange({name: 'password', value: text})}
                            />
                    
                            <View style={styles.buttonContainer} >
                                <LoginButton 
                                    title="GET STARTED" 
                                    onPress={() => {getStartedFunc()}}
                                    textStyle={{color: colors.white}}
                                    buttonStyle={{backgroundColor: colors.primary, marginBottom: 15}}
                                />

                                <Text style={styles.alreadyText1} >ALREADY HAVE AN ACCOUNT? 
                                
                                    <TouchableOpacity onPress={() => {navigation.navigate('Login')}} >
                                    <Text style={styles.alreadyText2} > LOG IN</Text>
                                    </TouchableOpacity>
                                </Text>
                            </View>
                    </View>
                    
                </ScrollView>
        
            </KeyboardAvoidingView>
      </SafeAreaView>
    )
}
 

const styles = StyleSheet.create({
    container: {
        width: screenWidth,
        alignItems: 'center'
    },
    textContent: {
        width: screenWidth-50,
    },
    heading: {
        fontSize: fontSize.Heading,
        fontFamily: Poppins.Bold,
        color: colors.black,
        marginBottom: 50,
        marginTop: 50,
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 50
    },
    alreadyText1: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Light,
        color: colors.gray,
    },
    alreadyText2: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Light,
        color: colors.primary,
    },
})

export default SignUpScreen
