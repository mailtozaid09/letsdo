import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, Alert, Modal, Pressable, } from 'react-native';

import { media } from '../../global/media';
import { colors } from '../../global/colors';
import { screenHeight, screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';


import BackButton from '../../components/button/BackButton';
import LoginButton from '../../components/button/LoginButton';

import Input from '../../components/input';



const ProfileScreen = (props) => {

    const navigation = props.navigation;


    useEffect(() => {
            console.log("props => taskData==  " );
    }, [])
    

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: colors.white}} >
            <View style={{padding: 20,flex: 1,}} >
                
                <View style={styles.headerContainer} >
                    <BackButton onPress={() => {navigation.goBack()}} />

                    <Text style={styles.heading} >Edit Profile</Text>

                    {/* <TouchableOpacity onPress={() => {setMenuOptions(true)}} >
                        <Image source={media.dots} style={{height: 30, width: 20}} />
                    </TouchableOpacity> */}

                    <Text style={{width: 30}} ></Text>
                </View>


                <View style={{alignItems: 'center', flex: 1, justifyContent: 'space-between'}} >

                  <View style={{alignItems: 'center'}} >
               
                    <Input
                        //form={form.name}
                        value="Zaid Ahmed"
                        placeholder="Name"
                        label="FULL NAME"
                        onChangeText={(text) => onChange({name: 'name', value: text})}
                    />

                    <Input
                        //form={form.email}
                        value="mailtozaid09@gmail.com"
                        placeholder="example@abc.com"
                        label="EMAIL"
                        onChangeText={(text) => onChange({name: 'email', value: text})}
                    />


                  </View>

                    <View>
                        <LoginButton
                           title="Edit Profile" 
                            onPress={() => {navigation.goBack()}}
                            textStyle={{color: colors.primary}}
                            buttonStyle={{borderColor: colors.primary, borderWidth: 1, marginBottom: 0}}
                        />
                    </View>
                </View>

            </View>

         </SafeAreaView>
    )
}
 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: screenWidth,
        alignItems: 'center'
    },
    textContent: {
        width: screenWidth-50,
    },
    heading: {
        fontSize: fontSize.SubHeading,
        fontFamily: Poppins.Bold,
        color: colors.black,
    },
    headerContainer: { 
        marginBottom: 30,
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    dateTimeContainer: {
        borderRadius: 50,
        padding: 5,
        paddingHorizontal: 15,
        marginBottom: 20,
        backgroundColor: colors.primary
    },
    taskName: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    dateTimeText: {
        fontSize: 12,
        fontFamily: Poppins.Light,
        color: colors.white,
    }
   

})

export default ProfileScreen
