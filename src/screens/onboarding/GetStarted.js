import React, { useState } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, } from 'react-native';
import LoginButton from '../../components/button/LoginButton';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';

const GetStartedScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.textContent} >
                <Text style={styles.title} >Let's Do</Text>
                
                <Image
                    source={media.getstarted}
                    style={styles.imageStyle}
                />

                <Text style={styles.welcomeTitle} >What are what we do</Text>
                <Text style={styles.description} >Thousand of people are using Let's Do for make To Do List for better productivity</Text>
            </View>

           

            <View style={styles.buttonContainer} >
                <LoginButton 
                    title="SIGN UP" 
                    onPress={() => {navigation.navigate('SignUp')}}
                    textStyle={{color: colors.white}}
                    buttonStyle={{backgroundColor: colors.primary, marginBottom: 15}}
                />

                <Text style={styles.alreadyText1} >ALREADY HAVE AN ACCOUNT? 
                
                    <TouchableOpacity onPress={() => {navigation.navigate('Login')}} >
                    <Text style={styles.alreadyText2} > LOG IN</Text>
                    </TouchableOpacity>
                </Text>
            </View>
            
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 12,
        backgroundColor: colors.white
    },
    textContent: {
        alignItems: 'center',
        margin: 40,
    },
    title: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.primary,
        textAlign: 'center',
        marginBottom: 30,
    },
    welcomeTitle: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Bold,
        color: colors.primary,
        textAlign: 'center',
        marginBottom: 10
    },
    description: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Light,
        color: colors.gray,
        textAlign: 'center',
    },
    imageStyle: {
        height: 240,
        marginTop: 50,
        marginBottom: 50,
        resizeMode: 'contain'
    },
    buttonContainer: {
        alignItems: 'center'
    },
    alreadyText1: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Light,
        color: colors.gray,
    },
    alreadyText2: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Light,
        color: colors.primary,
    },
})

export default GetStartedScreen