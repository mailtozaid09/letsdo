import React, { useState } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, } from 'react-native';
import LoginButton from '../../components/button/LoginButton';
import { colors } from '../../global/colors';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';

const OnBoardingScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.textContent} >
                <Text style={styles.title} >Let's Do</Text>
                <Text style={styles.welcomeTitle} >Hi! Welcome to</Text>
                <Text style={styles.welcomeSubTitle} >Let's Do</Text>
                <Text style={styles.description} >Explore the app, Let's make to do list with Let's Do</Text>

                <Image
                    source={media.onboarding}
                    style={styles.imageStyle}
                />
            </View>

           

            <LoginButton 
                title="GET STARTED" 
                onPress={() => {navigation.navigate('GetStarted')}}
                textStyle={{color: colors.primary}}
                buttonStyle={{backgroundColor: colors.white}}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 12,
        backgroundColor: colors.primary
    },
    textContent: {
        alignItems: 'center',
        margin: 40,
    },
    title: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: colors.white,
        textAlign: 'center',
        marginBottom: 30,
    },
    welcomeTitle: {
        fontSize: fontSize.Heading,
        fontFamily: Poppins.Medium,
        color: colors.white,
        textAlign: 'center',
    },
    welcomeSubTitle: {
        fontSize: fontSize.Heading,
        fontFamily: Poppins.Light,
        color: colors.white,
        textAlign: 'center',
        marginBottom: 20
    },
    description: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Light,
        color: colors.white,
        textAlign: 'center',
    },
    imageStyle: {
        height: 240,
        marginTop: 50,
        resizeMode: 'contain'
    },
})

export default OnBoardingScreen