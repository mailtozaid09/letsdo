
import React, {useState, useEffect, useRef} from 'react'

import { View, Text, SafeAreaView, StyleSheet, Image, TouchableOpacity, ScrollView,  } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize, Poppins } from '../../global/fontFamily'

import { addTodoList } from '../../redux'
import { useDispatch, useSelector } from 'react-redux'

import Input from '../../components/input'
import AddCategorySheet from '../../components/bottomSheet/AddCategorySheet'


import moment from 'moment'

const HomeScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const refRBSheet = useRef();

    const [taskArr, setTaskArr] = useState([]);

    const [searchValue, setSearchValue] = useState('');


    const todo_list = useSelector(state => state.HomeReducer?.todo_list);

    console.log("todo_list", todo_list);
    useEffect(() => {  

    }, [])


  

    const searchFilterFunction = (text) => {

        var todoListData = todo_list
        if (text) {
          const newData = todoListData.filter(
            function (item) {
              const itemData = item.taskName
                ? item.taskName.toUpperCase()
                : ''.toUpperCase();
              const textData = text.toUpperCase();
              return itemData.indexOf(textData) > -1;
          });

          
          setTaskArr(newData);
          setSearchValue(text);
        } else {

          setTaskArr(todoListData);
          setSearchValue(text);
        }
      };


    const addTodoCategory = (category, todoTaskName) => {
        const newdate = new Date();

        var date = moment(newdate).format("dddd, MMM Do YYYY");
        var time = moment(newdate).format("h:mm");

        dispatch(addTodoList({
            time: time,
            date: date,
            taskName: todoTaskName,
            category: category,
            isCompleted: false,

        }))
        refRBSheet.current.close()
    }



    const groupTodoByCategory = (todoArray) => {

        console.log('====================================');
        console.log("todoArray ", todoArray);
        console.log('====================================');
        const groups = todoArray?.reduce((groups, item) => ({
            ...groups,
            [item.category]: [...(groups[item.category] || []), item]
        }), {});

        return groups
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
            <View style={{padding: 20}} >

           
                
                <View style={{marginBottom: 20}} >
                    <Input
                        isSearch={true}
                        placeholder="Search"
                        value={searchValue}
                        onClearSearchText={() => setSearchValue('')}
                        onChangeText={(text) =>  {searchFilterFunction(text)}}
                    />
                </View>

               
                {searchValue
                ?
                <View>
                    {taskArr && taskArr.length != 0 ? taskArr.map((item) => (
                        <>
                       {item.category != 'Add' &&
                      
                        <TouchableOpacity style={styles.taskItemcontainer} >
                            <Image 
                             source={
                                item.category == 'Daily Tasks' 
                                ? media.daily 
                                : item.category == 'School Tasks' 
                                ? media.school 
                                : item.category == 'Shopping' 
                                ? media.cart 
                                : media.heart 
                            } 
                             style={{height: 26, width: 26, marginHorizontal: 20}} />
                            <Text style={styles.taskItemcontainerText} >{item.taskName}</Text>
                        </TouchableOpacity>}
                        </>
                    ))
                    :
                    <View style={{height: 300, width: '100%', alignItems: 'center', justifyContent: 'center'}} >
                        <Image source={media.oops} style={{height: 250, width: 250, resizeMode: 'contain'}} />
                        <Text style={{fontSize: fontSize.Heading, fontFamily: Poppins.Light, }} >No Task Available!</Text>
                    </View>
                }
                </View>
                :
                <View style={styles.categoryContainer} >
                {todo_list ? 
                <>
                    {Object.keys(groupTodoByCategory(todo_list)).map((category, index) => {
                        var categoryTask = groupTodoByCategory(todo_list)[category]

                        console.log("categoryTask ",categoryTask);
                        return(
                            <View>
                                <TouchableOpacity 
                                    onPress={() => {
                                        navigation.navigate('TaskStack', {
                                            screen: 'Tasks', 
                                            params: {task: categoryTask, categoryName: category}
                                        });
                                    }}
                                    style={[styles.categoryItem,  {backgroundColor: colors.primary}]} >
                                    <View style={styles.categoryIconContainer} >
                                        <Image 
                                        source={
                                            category == 'Daily Tasks' 
                                            ? media.daily 
                                            : category == 'School Tasks' 
                                            ? media.school 
                                            : category == 'Shopping' 
                                            ? media.school 
                                            : category == 'Work' 
                                            ? media.heart 
                                            : media.daily 
                                        } style={styles.categoryIcon} />
                                    </View>
                                    <Text style={styles.categoryTitle} >{category}</Text>
                                </TouchableOpacity>  
                            </View>
                        )
                    })}
                    </>
                :
                <Text>{todo_list?.length}</Text>
                }

                <TouchableOpacity 
                    onPress={() => refRBSheet.current.open()} 
                    style={[styles.categoryItem,  {borderColor: colors.primary, borderWidth: 2,}]} >
                    <View style={[styles.categoryIconContainer, {marginBottom: 0}]} >
                        <Image source={media.plus} style={styles.categoryIcon} />
                    </View>
                </TouchableOpacity>
        
            </View>

                }

            </View>
            </ScrollView>

            <AddCategorySheet refRBSheet={refRBSheet} addNewCategory={(category, todoTaskName) => addTodoCategory(category, todoTaskName)} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    heading: {
        fontSize: fontSize.Heading,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginBottom: 50
    },
    categoryContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    categoryItem: {
        height: screenWidth/2-30,
        width: screenWidth/2-30,
        marginBottom: 20,
        borderRadius: 10,
        alignItems: 'center', 
        justifyContent: 'center'
    },
    categoryIconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white
    },
    categoryIcon: {
        height: 35, 
        width: 35
    },
    categoryTitle: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    taskItemcontainer: {
        height: 60,
        backgroundColor: colors.white,
        borderWidth: 2,
        borderColor: colors.primary,
        borderRadius: 10,
        width: screenWidth-40,
        marginBottom: 15,
        alignItems: 'center',

        flexDirection: 'row'
    },
    taskItemcontainerText:{
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.SemiBold,
        color: colors.primary,
      
    }
})

export default HomeScreen