import React from 'react'
import { Text, View, Image, SafeAreaView } from 'react-native'
import LoginButton from '../components/button/LoginButton'
import { colors } from '../global/colors'

const RoughScreen = () => {
    return (
        <SafeAreaView style={{alignItems: 'center', justifyContent: 'center', flex: 1, backgroundColor: colors.primary}} >
            {/* <View>
                <Image source={require('../../assets/images/app_icon.png')} style={{height: 150, width: 150}} />
                <Text style={{fontSize: 40, color: colors.white, fontWeight: 'bold'}} >Let's Do</Text>
            </View> */}

            <LoginButton
                title="Get Started"
            />
        </SafeAreaView>
    )
}

export default RoughScreen